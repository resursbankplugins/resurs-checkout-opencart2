<?php

include(DIR_SYSTEM . '/library/resurs-ecomphp/vendor/autoload.php');

use Resursbank\Ecommerce\Types\Callback;
use Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES;
use Resursbank\RBEcomPHP\RESURS_ENVIRONMENTS as ResursEnvironments;
use Resursbank\RBEcomPHP\RESURS_FLOW_TYPES;
use Resursbank\RBEcomPHP\ResursBank;

function dd($item)
{
    echo "<pre>";
    var_dump($item);
    echo "</pre>";
    die;
}

class ControllerExtensionPaymentResursCheckout extends Controller
{
    private $errors = [];
    private $oldInput = [];
    private $success = null;
    private $settingsGroup = 'resurs_checkout';
    private $settingPrefix = 'resurs_checkout_';
    private $orderStatuses = ['on_hold', 'processing', 'completed', 'failed', 'cancelled', 'refunded'];

    public function index()
    {
        // Load in models
        $this->load->language('extension/payment/resurs_checkout');
        $this->load->model('localisation/order_status');
        $this->load->model('localisation/language');

        // If the user has sumbitted the form
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            $this->save();
        }

        $data = [];
        $data['token_name'] = version_compare(VERSION, '3.0', '<') ? 'token' : 'user_token';
        $data['errors'] = $this->errors;
        $data['oldInput'] = $this->oldInput;
        $data['success'] = $this->success;
        $data['languages'] = $this->model_localisation_language->getLanguages();
        $data['resursOrderStatuses'] = $this->orderStatuses;
        $data['orderStatuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $data['user_token'] = $this->session->data[$data['token_name']];
        $data['cancel'] = $this->url->link(
            'extension/extension',
            $data['token_name'] . '=' . $this->session->data[$data['token_name']] . '&type=payment',
            true
        );

        // Input
        $data['input'] = [
            'accounts' => $this->accountsInput(),
            'settings' => $this->settingsInput(),
        ];

        // Translations
        $data = array_merge($data, $this->translations());

        // Breadcrumbs
        $data['breadcrumbs'] = [
            [
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link(
                    'common/dashboard',
                    $data['token_name'] . '=' . $this->session->data[$data['token_name']],
                    true
                ),
            ],
            [
                'text' => $this->language->get('text_extension'),
                'href' => $this->url->link(
                    'extension/extension',
                    $data['token_name'] . '=' . $this->session->data[$data['token_name']] . '&type=payment',
                    true
                ),
            ],
            [
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link(
                    'extension/payment/resurs_checkout',
                    $data['token_name'] . '=' . $this->session->data[$data['token_name']],
                    true
                ),
            ],
        ];

        // Components
        $data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/payment/resurs_checkout', $data));
    }

    /**
     * Update the callbacks for an account.
     *
     * @return void
     */
    public function updateCallbacks()
    {
        $this->load->language('extension/payment/resurs_checkout');
        $response = ['error' => false, 'message' => ''];
        $accountIndex = $this->request->post['accountIndex'];
        $account = $this->getAccount($accountIndex);

        $callbacksResponse = $this->setCallbacksForAccount($account);

        if (!$callbacksResponse['success']) {
            $response['error'] = true;

            $response['message'] = $callbacksResponse['code'] == 401 ?
                $this->language->get('msg_credentials_failed') :
                $this->language->get('msg_set_callbacks_failed');

            $this->response->addHeader(
                $callbacksResponse['code'] == 401 ?
                    "HTTP/1.0 401 Unauthorized" :
                    "HTTP/1.0 400 Bad Request"
            );
        } else {
            $response['message'] = $this->language->get('msg_update_callbacks_success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

    /**
     * List the callbacks that are set for an account.
     *
     * @return void
     */
    public function listCallbacks()
    {
        $this->load->language('extension/payment/resurs_checkout');
        $response = ['error' => false, 'message' => ''];
        $accountIndex = $this->request->post['accountIndex'];
        $account = $this->getAccount($accountIndex);

        $callbacksResponse = $this->listCallbacksForAccount($account);

        if (!$callbacksResponse['success']) {
            $response['error'] = true;
            $response['message'] = $this->language->get('msg_listing_callbacks_failed');
            $this->response->addHeader("HTTP/1.0 500 Internal Server Error");
        } else {
            $response['message'] = !empty($callbacksResponse['callbacks']) ?
                $this->language->get('msg_listing_callbacks') :
                $this->language->get('msg_listing_callbacks_empty');
            $response['callbacks'] = $callbacksResponse['callbacks'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

    /**
     * Get the part payment settings for an account.
     *
     * @return void
     */
    public function partPayment()
    {
        $this->load->language('extension/payment/resurs_checkout');
        $accountIndex = $this->request->post['accountIndex'];
        $account = $this->getAccount($accountIndex);
        $response = ['error' => false, 'message' => ''];

        try {
            $data = [];
            $connector = $this->connectorInstance($account);
            $paymentMethods = $connector->getPaymentMethodsByAnnuity();

            foreach ($paymentMethods as $index => $paymentMethod) {
                $data[$index]['id'] = $paymentMethod->id;
                $data[$index]['description'] = $paymentMethod->description;
                $data[$index]['maxLimit'] = $paymentMethod->maxLimit;
                $data[$index]['annuityFactors'] = $connector->getAnnuityFactors($paymentMethod->id);
            }

            $response['data'] = $data;
            $response['storedData'] = $account['part_payment'];
            $response['translations'] = [
                'active' => $this->language->get('accounts_active'),
                'text_yes' => $this->language->get('text_yes'),
                'text_no' => $this->language->get('text_no'),
                'part_payment_method' => $this->language->get('part_payment_method'),
                'part_payment_duration' => $this->language->get('part_payment_duration'),
                'part_payment_min_limit' => $this->language->get('part_payment_min_limit'),
                'part_payment_max_limit' => $this->language->get('part_payment_max_limit'),
            ];
        } catch (\Exception $e) {
            $this->response->addHeader("HTTP/1.0 500 Internal Server Error");
            $response = ['error' => true, 'message' => $this->language->get('part_payment_fetch_failed')];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

    /**
     * Install the plugin
     *
     * @return void
     */
    public function install()
    {
        $this->load->model('setting/setting');

        $this->model_setting_setting->editSetting($this->settingsGroup, [
            $this->settingPrefix . 'status' => 1,
            $this->settingPrefix . 'salt' => uniqid(mt_rand(), true),
            $this->settingPrefix . 'environment' => 'test',
            $this->settingPrefix . 'accounts' => [
                1 => [
                    'note' => 'test',
                    'username' => '',
                    'password' => '',
                    'for_language' => '',
                    'active' => true,
                    'valid' => false,
                    'part_payment' => [
                        'active' => false,
                        'method' => '',
                        'duration' => '',
                        'min_limit' => 150.00,
                        'max_limit' => 0.0,
                        'factor' => 0.0,
                    ],
                ],
            ],
        ]);
    }

    /**
     * Uninstall the plugin
     *
     * @return void
     */
    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting($this->settingsGroup);
    }

    /**
     * Get the input to display for the accounts. If there are errors we'll show the old input,
     * otherwise the input that is stored in the database.
     *
     * @return array
     */
    private function accountsInput()
    {
        $settings = $this->getSettings();
        $accountsInput = [];
        $accountSettings = [];

        if (!empty($this->errors)) {
            // Collect the account settings
            $accountSettings['environment'] = isset($this->request->post['resurs_checkout_environment']) ?
                $this->request->post['resurs_checkout_environment'] :
                'test';

            // Collect the accounts
            foreach ($this->request->post['resurs_checkout_accounts'] as $index => $account) {
                $accountsInput[$index] = [
                    'note' => isset($this->request->post['resurs_checkout_accounts'][$index]['note']) ?
                        $this->request->post['resurs_checkout_accounts'][$index]['note'] :
                        'test',
                    'username' => isset($this->request->post['resurs_checkout_accounts'][$index]['username']) ?
                        $this->request->post['resurs_checkout_accounts'][$index]['username'] :
                        '',
                    'password' => isset($this->request->post['resurs_checkout_accounts'][$index]['password']) ?
                        $this->request->post['resurs_checkout_accounts'][$index]['password'] :
                        '',
                    'for_language' => isset($this->request->post['resurs_checkout_accounts'][$index]['for_language']) ?
                        $this->request->post['resurs_checkout_accounts'][$index]['for_language'] :
                        '',
                    'active' => isset($this->request->post['resurs_checkout_accounts'][$index]['active']) ?
                        (bool)$this->request->post['resurs_checkout_accounts'][$index]['active'] :
                        '',
                ];

                if (
                    !empty($accountsInput[$index]['username']) &&
                    !empty($accountsInput[$index]['password']) &&
                    !empty($accountsInput[$index]['for_language']) &&
                    (bool)$accountsInput[$index]['active'] == true
                ) {
                    $accountsInput[$index]['valid'] = true;
                } else {
                    $accountsInput[$index]['valid'] = false;
                }
            }
        } else {
            // Get the account settings
            $accountSettings['environment'] = $settings[$this->settingPrefix . 'environment'];

            $accountsInput = $settings[$this->settingPrefix . 'accounts'];

            // If we have a successful new save. We should also list the accounts callbacks.
            if ($this->success) {
                foreach ($accountsInput as $index => $account) {
                    if ($account['active']) {
                        $callbacks = $this->listCallbacksForAccount($account);
                        $accountsInput[$index]['callbacks'] = $callbacks['callbacks'];
                    }
                }
            }
        }

        return ['settings' => $accountSettings, 'accounts' => $accountsInput];
    }

    /**
     * Get the input to display for the settings. If there are error we'll show the old input,
     * otherwise the input that is stored in the database.
     *
     * @return array
     */
    private function settingsInput()
    {
        $settings = $this->getSettings();
        $settingsInput = [];

        if (!empty($this->errors)) {
            // Order statuses
            foreach ($this->orderStatuses as $status) {
                if (isset($this->request->post['resurs_checkout_settings']['order_statuses'][$status])) {
                    $postedStatus = $this->request->post['resurs_checkout_settings']['order_statuses'][$status];
                    $settingsInput['order_statuses'][$status] = $postedStatus;
                } else {
                    $settingsInput['order_statuses'][$status] = '';
                }
            }

            return $settingsInput;
        }

        foreach ($this->orderStatuses as $status) {
            if (isset($settings[$this->settingPrefix . "settings_order_statuses_{$status}"])) {
                $settingStatus = $settings[$this->settingPrefix . "settings_order_statuses_{$status}"];
                $settingsInput['order_statuses'][$status] = $settingStatus;
            } else {
                $settingsInput['order_statuses'][$status] = '';
            }
        }

        return $settingsInput;
    }

    /**
     * Validate the form request
     *
     * @return boolean
     */
    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/payment/resurs_checkout')) {
            $this->errors[] = $this->language->get('validation_no_permissions');
            return false;
        }

        $this->oldInput = $this->request->post;

        return $this->validateAccounts() & $this->validateSettings();
    }

    /**
     * Validate the accounts and it's corresponding settings.
     *
     * @return boolean
     */
    private function validateAccounts()
    {
        // Validate the environment that the accounts are at.
        if (empty($this->request->post['resurs_checkout_environment'])) {
            $this->errors['accounts']['settings']['environment'] = $this->language->get('validation_environment_required');
        }

        // Validate all of the accounts.
        foreach ($this->request->post['resurs_checkout_accounts'] as $index => $account) {
            if (empty($account['username'])) {
                $this->errors['accounts']['accounts'][$index]['username'] = $this->language->get('validation_username_required');
            }

            if (empty($account['password'])) {
                $this->errors['accounts']['accounts'][$index]['password'] = $this->language->get('validation_password_required');
            }

            if (empty($account['for_language'])) {
                $this->errors['accounts']['accounts'][$index]['for_language'] = $this->language->get('validation_for_language_required');
            }

            if (!isset($account['active'])) {
                $this->errors['accounts']['accounts'][$index]['active'] = $this->language->get('validation_active_required');
            }

            // Only run this check if the correct input is provided
            if (
                !empty($account['username']) &&
                !empty($account['password']) &&
                !empty($account['for_language']) &&
                (bool)$account['active']
            ) {
                if (!$this->isValidAccount($account, $this->request->post['resurs_checkout_environment'])) {
                    $this->errors['accounts']['accounts'][$index]['invalid'] = $this->language->get('validation_account_not_valid');
                }
            }

        }

        return empty($this->errors['accounts']);
    }

    /**
     * Validate all of the settings.
     *
     * @return boolean
     */
    private function validateSettings()
    {
        // Order statuses
        foreach ($this->orderStatuses as $status) {
            if (empty($this->request->post['resurs_checkout_settings']['order_statuses'][$status])) {
                $orderStatusErrorMsg = $this->language->get("validation_order_statuses_{$status}_required");
                $this->errors['settings']['order_statuses'][$status] = $orderStatusErrorMsg;
            }
        }

        return empty($this->errors['settings']);
    }

    /**
     * Save the input into the database
     *
     * @return boolean
     */
    private function save()
    {
        $settings = $this->getSettings();

        // Data to persist
        $data = [
            $this->settingPrefix . 'status' => 1,
            $this->settingPrefix . 'salt' => $settings[$this->settingPrefix . 'salt'],
            $this->settingPrefix . 'environment' => $this->request->post['resurs_checkout_environment'],
        ];

        // Accounts data
        $index = 1;
        $accounts = [];
        foreach ($this->request->post['resurs_checkout_accounts'] as $account) {
            $accounts[$index] = $account;
            $accounts[$index]['active'] = (bool)$account['active'];
            $accounts[$index]['valid'] = true;

            if (isset($account['part_payment'])) {
                $accounts[$index]['part_payment'] = $account['part_payment'];
                $accounts[$index]['part_payment']['active'] = (bool)$account['part_payment']['active'];
            } else {
                $acc = $settings[$this->settingPrefix . 'accounts'];

                if (isset($acc[$index])) {
                    $accounts[$index]['part_payment'] = $acc[$index]['part_payment'];
                } else {
                    $accounts[$index]['part_payment'] = [
                        'active' => false,
                        'method' => '',
                        'duration' => '',
                        'min_limit' => 150.00,
                        'max_limit' => 0.0,
                        'factor' => 0.0,
                    ];
                }
            }

            $index++;
        }
        $data[$this->settingPrefix . 'accounts'] = $accounts;

        // Settings - Order statuses
        foreach ($this->orderStatuses as $status) {
            $orderStatus = $this->request->post['resurs_checkout_settings']['order_statuses'][$status];
            $data[$this->settingPrefix . "settings_order_statuses_{$status}"] = $orderStatus;
        }

        $this->model_setting_setting->editSetting($this->settingsGroup, $data);

        $this->success['message'] = $this->language->get('msg_save_success');

        return true;
    }

    /**
     * Make a call to Resurs Bank and set the callback url's for an account.
     *
     * @param array $account
     * @return    array
     */
    private function setCallbacksForAccount($account)
    {
        try {
            $rcConnector = $this->connectorInstance($account);
            $url = new Url(HTTP_CATALOG, $this->config->get('config_secure') ? HTTP_CATALOG : HTTPS_CATALOG);
            $urlPattern = $url->link('extension/payment/resurs_checkout/callbacks') .
                '&action={type}&paymentId={paymentId}&digest={digest}';

            // Now set the callbacks
            $rcConnector->setRegisterCallback(
                Callback::BOOKED,
                str_replace('{type}', 'booked', $urlPattern)
            );
            $rcConnector->setRegisterCallback(
                Callback::FINALIZATION,
                str_replace('{type}', 'finalization', $urlPattern)
            );
            $rcConnector->setRegisterCallback(
                Callback::UNFREEZE,
                str_replace('{type}', 'unfreeze', $urlPattern)
            );
            $rcConnector->setRegisterCallback(
                Callback::ANNULMENT,
                str_replace('{type}', 'annulment', $urlPattern)
            );
            $rcConnector->setRegisterCallback(
                Callback::AUTOMATIC_FRAUD_CONTROL,
                str_replace(
                    '{type}',
                    'fraud_control',
                    $urlPattern . '&result={result}'
                )
            );
            $rcConnector->setRegisterCallback(
                Callback::UPDATE,
                str_replace('{type}', 'update', $urlPattern)
            );
            $rcConnector->setRegisterCallback(
                Callback::TEST,
                $url->link('extension/payment/resurs_checkout/callbacks') .
                '&event-type=TEST&par1={param1}&par2={param2}&par3={param3}&par4={param4}&par5={param5}'
            );
        } catch (\Exception $e) {
            return ['success' => false, 'code' => $e->getCode()];
        }

        return ['success' => true];
    }

    /**
     * MAke a call to Resurs Bank to list the callbacks that are set for an account.
     *
     * @param array $account
     * @return    array
     */
    private function listCallbacksForAccount($account)
    {
        try {
            $rcConnector = $this->connectorInstance($account);

            $callbacks = $rcConnector->getCallBacksByRest(true);
        } catch (\Exception $e) {
            return ['success' => false];
        }

        return ['success' => true, 'callbacks' => $callbacks];
    }

    /**
     * All of the translations that are used in the admin panel.
     *
     * @return array
     */
    private function translations()
    {
        $translations = [
            'button_save' => $this->language->get('button_save'),
            'button_cancel' => $this->language->get('button_cancel'),
            'heading_title' => $this->language->get('heading_title'),
            'text_edit' => $this->language->get('text_edit'),
            'text_test' => $this->language->get('text_test'),
            'text_production' => $this->language->get('text_production'),
            'text_callbacks' => $this->language->get('text_callbacks'),
            'text_update_callbacks' => $this->language->get('text_update_callbacks'),
            'text_choose' => $this->language->get('text_choose'),
            'text_button_add_account' => $this->language->get('text_button_add_account'),
            'text_yes' => $this->language->get('text_yes'),
            'text_no' => $this->language->get('text_no'),
            'tab_accounts' => $this->language->get('tab_accounts'),
            'tab_settings' => $this->language->get('tab_settings'),
            'accounts_environment' => $this->language->get('accounts_environment'),
            'accounts_for_language' => $this->language->get('accounts_for_language'),
            'accounts_username' => $this->language->get('accounts_username'),
            'accounts_password' => $this->language->get('accounts_password'),
            'accounts_active' => $this->language->get('accounts_active'),
            'accounts_actions' => $this->language->get('accounts_actions'),
            'accounts_tab_part_payment' => $this->language->get('accounts_tab_part_payment'),
            'accounts_tab_callbacks' => $this->language->get('accounts_tab_callbacks'),
            'accounts_list_callbacks' => $this->language->get('accounts_list_callbacks'),
            'accounts_update_callbacks' => $this->language->get('accounts_update_callbacks'),
            'accounts_env_test' => $this->language->get('accounts_env_test'),
            'accounts_env_production' => $this->language->get('accounts_env_production'),
            'part_payment_method' => $this->language->get('part_payment_method'),
            'part_payment_duration' => $this->language->get('part_payment_duration'),
            'part_payment_min_limit' => $this->language->get('part_payment_min_limit'),
            'part_payment_max_limit' => $this->language->get('part_payment_max_limit'),
            'help_update_callbacks' => $this->language->get('help_update_callbacks'),
            'validation_errors' => $this->language->get('validation_errors'),
            'validation_no_permissions' => $this->language->get('validation_no_permissions'),
            'settings_order_status_heading' => $this->language->get('settings_order_status_heading'),
            'msg_listing_callbacks' => $this->language->get('msg_listing_callbacks'),
            'msg_listing_callbacks_empty' => $this->language->get('msg_listing_callbacks_empty'),
            'msg_save_before_performing_action' => $this->language->get('msg_save_before_performing_action'),
        ];

        foreach ($this->orderStatuses as $status) {
            $translations['settings_order_status_' . $status] = $this->language->get('settings_order_status_' . $status);
            $translations['help_order_status_' . $status] = $this->language->get('help_order_status_' . $status);
        }

        return $translations;
    }

    /**
     * Get a connector instance to eCom-PHP that handles requests to Resurs Bank
     *
     * @param array $account
     * @return    Resursbank\RBEcomPHP\ResursBank
     * @throws Exception
     */
    private function connectorInstance($account)
    {
        $settings = $this->getSettings();
        $salt = $settings[$this->settingPrefix . 'salt'];
        $environment = $settings[$this->settingPrefix . 'environment'];

        $connector = new ResursBank($account['username'], $account['password']);
        $connector->setCallbackDigest($salt);
        $connector->setEnvironment(
            $environment == 'test' ?
                ResursEnvironments::TEST:
                ResursEnvironments::PRODUCTION
        );
        $connector->setPreferredPaymentFlowService(RESURS_FLOW_TYPES::RESURS_CHECKOUT);

        return $connector;
    }

    /**
     * Is the account we provided a valid account?
     *
     * @param array $account
     * @param string $environment
     * @return    boolean
     * @throws Exception
     */
    private function isValidAccount($account, $environment)
    {
        $connector = new ResursBank($account['username'], $account['password']);
        $connector->setEnvironment(
            $environment == 'test' ?
                ResursEnvironments::ENVIRONMENT_TEST :
                ResursEnvironments::ENVIRONMENT_PRODUCTION
        );

        try {
            $response = $connector->getPaymentMethods();
        } catch (\Exception $e) {
            if ($e->getCode() == 401) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get an account from the database based on the index.
     *
     * @param integer $index
     * @return    array|null
     */
    private function getAccount($index)
    {
        $settings = $this->getSettings();
        $accounts = $settings[$this->settingPrefix . 'accounts'];

        return isset($accounts[$index]) ? $accounts[$index] : null;
    }

    /**
     * Get the settings from the database.
     *
     * @return    array
     */
    private function getSettings()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getSetting($this->settingsGroup);
    }
}
