<?php

	$_['text_resurs_checkout'] = '<a onclick="window.open(\'https://www.resursbank.se/foretag/\');"><img src="view/image/payment/resurs_checkout.png" alt="Resurs Checkout" title="Resurs Checkout" style="border: 1px solid #EEEEEE;" /></a>';

	// Static text
	$_['heading_title']						= "Resurs Checkout_no";
	$_['text_payment']						= "Betalningar_no";
	$_['text_title']						= "Resurs Checkout Inställningar_no";
	$_['text_edit']							= "Redigera Resurs Checkout inställningar._no";
	$_['text_save']							= "Spara_no";
	$_['text_cancel']						= "Avbryt_no";
	$_['text_environment']					= "Miljö_no";
	$_['text_live']							= "Live_no";
	$_['text_test']							= "Test_no";
	$_['text_active'] 						= "Aktiv_no";
	$_['text_yes'] 							= "Ja_no";
	$_['text_no'] 							= "Nej_no";
	$_['text_username']						= "Användarnamn_no";
	$_['text_password']						= "Lösenord_no";
	$_['text_callback_url']					= "Callback Url_no";
	$_['text_shopurl']						= "Shop Url_no";
	$_['text_swedish_user']					= "Svenskt ombud_no";
	$_['text_norwegian_user']				= "Norskt ombud_no";
	$_['text_for_language']					= "För språk_no";
	$_['text_for_language_help']			= "Ombudet ska användas när detta språket är aktivt._no";
	$_['text_validation_error']				= "Validering misslyckades._no";
	$_['text_success']						= "Sparade dina inställningar._no";
	$_['text_for_country_id']				= "Valt land_no";
	$_['order_statuses']					= "Order statusar_no";
	$_['select_status']						= "Välj status_no";
	$_['voided_status']						= "Voided_no";
	$_['booked_status']						= "Booked_no";
	$_['annulment_status']					= "Annulment_no";
	$_['unfreeze_status']					= "Unfreeze_no";
	$_['frozen_status']						= "Frozen_no";
	$_['thawed_status']						= "Thawed_no";

	// Partial Payment text
	$_['partial_payment_heading']			= "Delbetalningsalternativ på produktsidan._no";
	$_['payment_method']					= "Betalningsalternativ_no";
	$_['payment_method_description']		= "Betalningsalternativ som används för att beräkna det föreslagna delbetalningspriset som visas på dina produktsidor._no";
	$_['payment_duration']					= "Löptid (mån)_no";
	$_['payment_duration_description']		= "Antal månader som föreslagna delbetalningspriset är baserade på. Du måste ange en betalmetod innan du kan ange detta värdet._no";
	$_['partial_payment_not_available']		= "För att välja delbetalningsalternativ för produktsidan måste man först ange användarnamn och lösenord och klicka på spara knappen längst ned på sidan._no";

	// Validation
	$_['validation_environment_required']		= "Miljö är obligatoriskt._no";
	$_['validation_environment_invalid']		= "Miljö är ej giltigt._no";
	$_['validation_username_se_required']		= "Användarnamn för svenskt ombud är obligatoriskt när ombudet är aktivt._no";
	$_['validation_password_se_required']		= "Lösenord för svenkst ombud är obligatoriskt när ombudet är aktivt._no";
	$_['validation_language_code_se_required']	= "Välj vilket språk som ska vara aktivt när det svenska ombudet ska vara aktivt._no";
	$_['validation_callback_url_required']  	= "Callback Url är obligatoriskt._no";
	$_['validation_shopurl_required']  			= "Shop Url är obligatoriskt._no";
	$_['validation_no_permissions']				= "Du har ej rättigheter till att redigera denna betalmetoden._no";
	$_['validation_voided_status_required']		= "Voided status är obligatoriskt._no";
	$_['validation_booked_status_required']		= "Booked status är obligatoriskt._no";
	$_['validation_annulment_status_required']	= "Annulment status är obligatoriskt._no";
	$_['validation_unfreeze_status_required']	= "Unfreeze status är obligatoriskt._no";
	$_['validation_frozen_status_required']		= "Frozen status är obligatoriskt._no";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt._no";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt._no";
	$_['validation_country_id_required']		= "Land är obligatoriskt._no";