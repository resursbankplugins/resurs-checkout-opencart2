<?php

$_['text_resurs_checkout'] = '<a onclick="window.open(\'https://www.resursbank.se/foretag/\');"><img src="view/image/payment/resurs_checkout.png" alt="Resurs Checkout" title="Resurs Checkout" style="border: 1px solid #EEEEEE;" /></a>';
$_['heading_title'] = 'Resurs Checkout';

// Text
$_['text_extension'] = 'Extensions';
$_['text_edit'] = 'Edit Resurs Checkout';
$_['text_test'] = 'Test';
$_['text_production'] = 'Production';
$_['text_callbacks'] = 'Callbacks';
$_['text_update_callbacks'] = 'Update your callbacks';
$_['text_choose'] = '-- Choose --';
$_['text_button_add_account'] = 'Add account';
$_['text_yes'] = 'Yes';
$_['text_no'] = 'No';
    
// Tab headings
$_['tab_settings'] = 'Settings';
$_['tab_accounts'] = 'Accounts';

// Accounts
$_['accounts_environment'] = 'Environment';
$_['accounts_for_language'] = 'For language';
$_['accounts_username'] = 'Username';
$_['accounts_password'] = 'Password';
$_['accounts_active'] = 'Active';
$_['accounts_actions'] = 'Actions';
$_['accounts_tab_part_payment'] = 'Part Payment';
$_['accounts_tab_callbacks'] = 'Callbacks';
$_['accounts_list_callbacks'] = 'List callbacks';
$_['accounts_update_callbacks'] = 'Update callbacks';
$_['accounts_env_test'] = 'Test';
$_['accounts_env_production'] = 'Production';

// Part Payment
$_['part_payment_method'] = 'Payment Method';
$_['part_payment_duration'] = 'Duration';
$_['part_payment_min_limit'] = 'Min Limit';
$_['part_payment_max_limit'] = 'Max Limit';
$_['part_payment_fetch_failed'] = 'Could not fetch the part payment settings. Please try again.';

// Settings
$_['settings_order_status_heading'] = 'Order statuses';
$_['settings_order_status_on_hold'] = 'Resurs On-Hold';
$_['settings_order_status_processing'] = 'Resurs Processing';
$_['settings_order_status_completed'] = 'Resurs Completed';
$_['settings_order_status_failed'] = 'Resurs Failed';
$_['settings_order_status_cancelled'] = 'Resurs Cancelled';
$_['settings_order_status_refunded'] = 'Resurs Refunded';

// Messages
$_['msg_account_not_set'] = 'You need to set your account before performing this action';
$_['msg_credentials_failed'] = 'Could not authenticate. Is your account settings correct?';
$_['msg_set_callbacks_failed'] = 'Could not set your callbacks. Please try again';
$_['msg_update_callbacks_success'] = 'Successfully updated your callbacks.';
$_['msg_listing_callbacks'] = 'Listing the registered callbacks for this account.';
$_['msg_listing_callbacks_empty'] = 'You have no registered callbacks. Click on the Update Callbacks button for this account to set them.';
$_['msg_listing_callbacks_failed'] = 'Could not list the callbacks for the account. Please try again or contact Resurs Bank support.';
$_['msg_save_success'] = 'Successfully saved your changes.';
$_['msg_save_before_performing_action'] = 'Before performing any actions on this account you first need to enter a valid resurs account and click the save button.';

// Help Tooltip
$_['help_update_callbacks'] = 'Update the callbacks. You need to save the environment, username and password that you want to use before updating.';
$_['help_order_status_on_hold'] = 'This is the tooltip for status Resurs On-Hold.';
$_['help_order_status_processing'] = 'This is the tooltip for status Resurs Processing.';
$_['help_order_status_completed'] = 'This is the tooltip for status Resurs Completed.';
$_['help_order_status_failed'] = 'This is the tooltip for status Resurs Failed.';
$_['help_order_status_cancelled'] = 'This is the tooltip for status Resurs Cancelled.';
$_['help_order_status_refunded'] = 'This is the tooltip for status Resurs Refunded.';

// Validation
$_['validation_errors'] = 'Validation Error!';
$_['validation_no_permissions'] = 'Not authorized on admin.';
$_['validation_environment_required'] = 'You need to set an environment.';
$_['validation_username_required'] = 'Username is a required field.';
$_['validation_password_required'] = 'Password is a required field.';
$_['validation_for_language_required'] = 'For language is a required field';
$_['validation_active_required'] = 'Active is a required field.';
$_['validation_account_not_valid'] = 'The account is invalid.';
$_['validation_order_statuses_on_hold_required'] = 'An order status for Resurs On-Hold is required.';
$_['validation_order_statuses_processing_required'] = 'An order status for Resurs Processing is required.';
$_['validation_order_statuses_completed_required'] = 'An order status for Resurs Completed is required.';
$_['validation_order_statuses_fail_required'] = 'An order status for Resurs Failed is required.';
$_['validation_order_statuses_cancelled_required'] = 'An order status for Resurs Cancelled is required.';
$_['validation_order_statuses_refunded_required'] = 'An order status for Resurs Refunded is required.';
