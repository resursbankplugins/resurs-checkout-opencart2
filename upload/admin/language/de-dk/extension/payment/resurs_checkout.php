<?php

	$_['text_resurs_checkout'] = '<a onclick="window.open(\'https://www.resursbank.se/foretag/\');"><img src="view/image/payment/resurs_checkout.png" alt="Resurs Checkout" title="Resurs Checkout" style="border: 1px solid #EEEEEE;" /></a>';

	// Static text
	$_['heading_title']						= "Resurs Checkout_dk";
	$_['text_payment']						= "Betalningar_dk";
	$_['text_title']						= "Resurs Checkout Inställningar_dk";
	$_['text_edit']							= "Redigera Resurs Checkout inställningar_dk.";
	$_['text_save']							= "Spara_dk";
	$_['text_cancel']						= "Avbryt_dk";
	$_['text_environment']					= "Miljö_dk";
	$_['text_live']							= "Live_dk";
	$_['text_test']							= "Test_dk";
	$_['text_active'] 						= "Aktiv_dk";
	$_['text_yes'] 							= "Ja_dk";
	$_['text_no'] 							= "Nej_dk";
	$_['text_username']						= "Användarnamn_dk";
	$_['text_password']						= "Lösenord_dk";
	$_['text_callback_url']					= "Callback Url_dk";
	$_['text_shopurl']						= "Shop Url_dk";
	$_['text_swedish_user']					= "Svenskt ombud_dk";
	$_['text_norwegian_user']				= "Norskt ombud_dk";
	$_['text_finnish_user']					= "Finskt ombud_dk";
	$_['text_danish_user']					= "Danskt ombud_dk";
	$_['text_for_language']					= "För språk_dk";
	$_['text_for_language_help']			= "Ombudet ska användas när detta språket är aktivt._dk";
	$_['text_validation_error']				= "Validering misslyckades._dk";
	$_['text_success']						= "Sparade dina inställningar._dk";
	$_['text_for_country_id']				= "Valt land_dk";
	$_['order_statuses']					= "Order statusar_dk";
	$_['select_status']						= "Välj status_dk";
	$_['voided_status']						= "Voided_dk";
	$_['booked_status']						= "Booked_dk";
	$_['annulment_status']					= "Annulment_dk";
	$_['unfreeze_status']					= "Unfreeze_dk";
	$_['frozen_status']						= "Frozen_dk";
	$_['thawed_status']						= "Thawed_dk";

	// Partial Payment text
	$_['partial_payment_heading']			= "Delbetalningsalternativ på produktsidan._dk";
	$_['payment_method']					= "Betalningsalternativ_dk";
	$_['payment_method_description']		= "Betalningsalternativ som används för att beräkna det föreslagna delbetalningspriset som visas på dina produktsidor._dk";
	$_['payment_duration']					= "Löptid (mån)_dk";
	$_['payment_duration_description']		= "Antal månader som föreslagna delbetalningspriset är baserade på. Du måste ange en betalmetod innan du kan ange detta värdet._dk";
	$_['partial_payment_not_available']		= "För att välja delbetalningsalternativ för produktsidan måste man först ange användarnamn och lösenord och klicka på spara knappen längst ned på sidan._dk";

	// Validation
	$_['validation_environment_required']		= "Miljö är obligatoriskt._dk";
	$_['validation_environment_invalid']		= "Miljö är ej giltigt._dk";
	$_['validation_username_se_required']		= "Användarnamn för svenskt ombud är obligatoriskt när ombudet är aktivt._dk";
	$_['validation_password_se_required']		= "Lösenord för svenkst ombud är obligatoriskt när ombudet är aktivt._dk";
	$_['validation_language_code_se_required']	= "Välj vilket språk som ska vara aktivt när det svenska ombudet ska vara aktivt._dk";
	$_['validation_callback_url_required']  	= "Callback Url är obligatoriskt._dk";
	$_['validation_shopurl_required']  			= "Shop Url är obligatoriskt._dk";
	$_['validation_no_permissions']				= "Du har ej rättigheter till att redigera denna betalmetoden._dk";
	$_['validation_voided_status_required']		= "Voided status är obligatoriskt._dk";
	$_['validation_booked_status_required']		= "Booked status är obligatoriskt._dk";
	$_['validation_annulment_status_required']	= "Annulment status är obligatoriskt._dk";
	$_['validation_unfreeze_status_required']	= "Unfreeze status är obligatoriskt._dk";
	$_['validation_frozen_status_required']		= "Frozen status är obligatoriskt._dk";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt._dk";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt._dk";
	$_['validation_country_id_required']		= "Land är obligatoriskt._dk";