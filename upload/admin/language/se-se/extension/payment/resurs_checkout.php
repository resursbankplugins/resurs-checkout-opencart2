<?php

	$_['text_resurs_checkout'] = '<a onclick="window.open(\'https://www.resursbank.se/foretag/\');"><img src="view/image/payment/resurs_checkout.png" alt="Resurs Checkout" title="Resurs Checkout" style="border: 1px solid #EEEEEE;" /></a>';

	// Static text
	$_['heading_title']						= "Resurs Checkout";
	$_['text_payment']						= "Betalningar";
	$_['text_title']						= "Resurs Checkout Inställningar";
	$_['text_edit']							= "Redigera Resurs Checkout inställningar.";
	$_['text_save']							= "Spara";
	$_['text_cancel']						= "Avbryt";
	$_['text_environment']					= "Miljö";
	$_['text_live']							= "Live";
	$_['text_test']							= "Test";
	$_['text_active'] 						= "Aktiv";
	$_['text_yes'] 							= "Ja";
	$_['text_no'] 							= "Nej";
	$_['text_username']						= "Användarnamn";
	$_['text_password']						= "Lösenord";
	$_['text_callback_url']					= "Callback Url";
	$_['text_shopurl']						= "Shop Url";
	$_['text_swedish_user']					= "Svenskt ombud";
	$_['text_norwegian_user']				= "Norskt ombud";
	$_['text_finnish_user']					= "Finskt ombud";
	$_['text_danish_user']					= "Danskt ombud";
	$_['text_for_language']					= "För språk";
	$_['text_for_language_help']			= "Ombudet ska användas när detta språket är aktivt.";
	$_['text_validation_error']				= "Validering misslyckades.";
	$_['text_success']						= "Sparade dina inställningar.";
	$_['text_for_country_id']				= "Valt land";
	$_['order_statuses']					= "Order statusar";
	$_['select_status']						= "Välj status";
	$_['voided_status']						= "Voided";
	$_['booked_status']						= "Booked";
	$_['annulment_status']					= "Annulment";
	$_['unfreeze_status']					= "Unfreeze";
	$_['frozen_status']						= "Frozen";
	$_['thawed_status']						= "Thawed";

	// Partial Payment text
	$_['partial_payment_heading']			= "Delbetalningsalternativ på produktsidan.";
	$_['payment_method']					= "Betalningsalternativ";
	$_['payment_method_description']		= "Betalningsalternativ som används för att beräkna det föreslagna delbetalningspriset som visas på dina produktsidor.";
	$_['payment_duration']					= "Löptid (mån)";
	$_['payment_duration_description']		= "Antal månader som föreslagna delbetalningspriset är baserade på. Du måste ange en betalmetod innan du kan ange detta värdet.";
	$_['partial_payment_not_available']		= "För att välja delbetalningsalternativ för produktsidan måste man först ange användarnamn och lösenord och klicka på spara knappen längst ned på sidan.";

	// Validation
	$_['validation_environment_required']		= "Miljö är obligatoriskt.";
	$_['validation_environment_invalid']		= "Miljö är ej giltigt.";
	$_['validation_username_se_required']		= "Användarnamn för svenskt ombud är obligatoriskt när ombudet är aktivt.";
	$_['validation_password_se_required']		= "Lösenord för svenkst ombud är obligatoriskt när ombudet är aktivt.";
	$_['validation_language_code_se_required']	= "Välj vilket språk som ska vara aktivt när det svenska ombudet ska vara aktivt.";
	$_['validation_callback_url_required']  	= "Callback Url är obligatoriskt.";
	$_['validation_shopurl_required']  			= "Shop Url är obligatoriskt.";
	$_['validation_no_permissions']				= "Du har ej rättigheter till att redigera denna betalmetoden.";
	$_['validation_voided_status_required']		= "Voided status är obligatoriskt.";
	$_['validation_booked_status_required']		= "Booked status är obligatoriskt.";
	$_['validation_annulment_status_required']	= "Annulment status är obligatoriskt.";
	$_['validation_unfreeze_status_required']	= "Unfreeze status är obligatoriskt.";
	$_['validation_frozen_status_required']		= "Frozen status är obligatoriskt.";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt.";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt.";
	$_['validation_country_id_required']		= "Land är obligatoriskt.";