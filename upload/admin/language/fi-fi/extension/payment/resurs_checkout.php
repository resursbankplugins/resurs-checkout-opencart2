<?php

	$_['text_resurs_checkout'] = '<a onclick="window.open(\'https://www.resursbank.se/foretag/\');"><img src="view/image/payment/resurs_checkout.png" alt="Resurs Checkout" title="Resurs Checkout" style="border: 1px solid #EEEEEE;" /></a>';

	// Static text
	$_['heading_title']						= "Resurs Checkout_fi";
	$_['text_payment']						= "Betalningar_fi";
	$_['text_title']						= "Resurs Checkout Inställningar_fi";
	$_['text_edit']							= "Redigera Resurs Checkout inställningar_fi.";
	$_['text_save']							= "Spara_fi";
	$_['text_cancel']						= "Avbryt_fi";
	$_['text_environment']					= "Miljö_fi";
	$_['text_live']							= "Live_fi";
	$_['text_test']							= "Test_fi";
	$_['text_active'] 						= "Aktiv_fi";
	$_['text_yes'] 							= "Ja_fi";
	$_['text_no'] 							= "Nej_fi";
	$_['text_username']						= "Användarnamn_fi";
	$_['text_password']						= "Lösenord_fi";
	$_['text_callback_url']					= "Callback Url_fi";
	$_['text_shopurl']						= "Shop Url_fi";
	$_['text_swedish_user']					= "Svenskt ombud_fi";
	$_['text_norwegian_user']				= "Norskt ombud_fi";
	$_['text_finnish_user']					= "Finskt ombud_fi";
	$_['text_danish_user']					= "Danskt ombud_fi";
	$_['text_for_language']					= "För språk_fi";
	$_['text_for_language_help']			= "Ombudet ska användas när detta språket är aktivt._fi";
	$_['text_validation_error']				= "Validering misslyckades._fi";
	$_['text_success']						= "Sparade dina inställningar._fi";
	$_['text_for_country_id']				= "Valt land_fi";
	$_['order_statuses']					= "Order statusar_fi";
	$_['select_status']						= "Välj status_fi";
	$_['voided_status']						= "Voided_fi";
	$_['booked_status']						= "Booked_fi";
	$_['annulment_status']					= "Annulment_fi";
	$_['unfreeze_status']					= "Unfreeze_fi";
	$_['frozen_status']						= "Frozen_fi";
	$_['thawed_status']						= "Thawed_fi";

	// Partial Payment text
	$_['partial_payment_heading']			= "Delbetalningsalternativ på produktsidan._fi";
	$_['payment_method']					= "Betalningsalternativ_fi";
	$_['payment_method_description']		= "Betalningsalternativ som används för att beräkna det föreslagna delbetalningspriset som visas på dina produktsidor._fi";
	$_['payment_duration']					= "Löptid (mån)_fi";
	$_['payment_duration_description']		= "Antal månader som föreslagna delbetalningspriset är baserade på. Du måste ange en betalmetod innan du kan ange detta värdet._fi";
	$_['partial_payment_not_available']		= "För att välja delbetalningsalternativ för produktsidan måste man först ange användarnamn och lösenord och klicka på spara knappen längst ned på sidan._fi";

	// Validation
	$_['validation_environment_required']		= "Miljö är obligatoriskt._fi";
	$_['validation_environment_invalid']		= "Miljö är ej giltigt._fi";
	$_['validation_username_se_required']		= "Användarnamn för svenskt ombud är obligatoriskt när ombudet är aktivt._fi";
	$_['validation_password_se_required']		= "Lösenord för svenkst ombud är obligatoriskt när ombudet är aktivt._fi";
	$_['validation_language_code_se_required']	= "Välj vilket språk som ska vara aktivt när det svenska ombudet ska vara aktivt._fi";
	$_['validation_callback_url_required']  	= "Callback Url är obligatoriskt._fi";
	$_['validation_shopurl_required']  			= "Shop Url är obligatoriskt._fi";
	$_['validation_no_permissions']				= "Du har ej rättigheter till att redigera denna betalmetoden._fi";
	$_['validation_voided_status_required']		= "Voided status är obligatoriskt._fi";
	$_['validation_booked_status_required']		= "Booked status är obligatoriskt._fi";
	$_['validation_annulment_status_required']	= "Annulment status är obligatoriskt._fi";
	$_['validation_unfreeze_status_required']	= "Unfreeze status är obligatoriskt._fi";
	$_['validation_frozen_status_required']		= "Frozen status är obligatoriskt._fi";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt._fi";
	$_['validation_thawed_status_required']		= "Thawed status är obligatoriskt._fi";
	$_['validation_country_id_required']		= "Land är obligatoriskt._fi";