#!/bin/bash
echo "Preparing library structure..."
echo "Clean up cache ..."
composer clear >/dev/null 2>&1
echo "Upgrading vendor by removal..."
rm -fr vendor source
composer install --prefer-dist
composer up
echo "Cleaning up unnecessary ignores ..."
find vendor/ -name .gitignore -exec rm -vf {} \; >/dev/null 2>&1
echo "Cleaning up .git structures ..."
find vendor/ -type d -name .git -exec rm -rvf {} \; >/dev/null 2>&1
echo "Cleaning up tests..."
find vendor/ -type d -name tests -exec rm -rvf {} \; >/dev/null 2>&1

