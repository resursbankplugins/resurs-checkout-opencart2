<?php
// Include eComPHP
include(DIR_SYSTEM . 'library/resurs-ecomphp/vendor/autoload.php');

use Resursbank\Ecommerce\Types\OrderStatus;
use Resursbank\RBEcomPHP\RESURS_ENVIRONMENTS as ResursEnvironments;
use Resursbank\RBEcomPHP\RESURS_FLOW_TYPES;
use Resursbank\RBEcomPHP\ResursBank;
use TorneLIB\Utils\Generic;

function dd($dump)
{
    echo '<pre>';
    var_dump($dump);
    die;
}

/**
 * @version 2.1.2
 */
class ControllerExtensionPaymentResursCheckout extends Controller
{
    private $settingPrefix = 'resurs_checkout_';
    /**
     * We cache the settings.
     */
    private $settings = null;
    /**
     * We cache the connector.
     */
    private $connector = null;
    /**
     * The reference for the frame at the moment
     */
    private $rcoReference = null;
    /**
     * We cache the totals for multiple use so we don't need to go down to the database
     */
    private $totals = null;

    /**
     * @throws Exception
     */
    public function index()
    {
        // If there is no products in the cart or any vouchers we should just redirect back to the cart.
        if (!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) {
            $this->response->redirect($this->url->link('checkout/cart'));
        }

        $this->load->language('checkout/cart');
        $this->load->language('extension/payment/resurs_checkout');

        $this->document->setTitle($this->language->get('resurs_checkout_title'));

        // Breadcrumbs
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = [
            'href' => $this->url->link('common/home'),
            'text' => $this->language->get('text_home'),
        ];

        $data['breadcrumbs'][] = [
            'href' => $this->url->link('checkout/cart'),
            'text' => $this->language->get('heading_title'),
        ];

        $data['breadcrumbs'][] = [
            'href' => $this->url->link('extension/payment/resurs_checkout'),
            'text' => $this->language->get('resurs_checkout_title'),
        ];

        // Do we have a flash error message?
        if (isset($this->session->data['error_flash_msg']) && !empty($this->session->data['error_flash_msg'])) {
            $data['error_flash_msg'] = $this->session->data['error_flash_msg'];
            unset($this->session->data['error_flash_msg']);
        }

        // Links
        $data['continue'] = $this->url->link('common/home');

        // Translations
        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['button_shopping'] = $this->language->get('button_shopping');
        $data['confirm_order'] = $this->language->get('confirm_order');

        // The total order display
        $data['products'] = $this->getProducts();
        $data['vouchers'] = $this->getVouchers();
        $data['totals'] = $this->getTotals();

        // The RCO iFrame and data needed.
        $data['rco'] = $this->rcoData();
        $data['reference'] = $this->rcoReference;
        $data['ajax_createOrderUrl'] = $this->url->link('extension/payment/resurs_checkout/createOrder');
        $data['ajax_failOrderUrl'] = $this->url->link('extension/payment/resurs_checkout/failOrder');
        $data['ajax_paymentSessionId'] = $data['rco']['paymentSessionId'];
        $data['ajax_baseUrl'] = $data['rco']['baseUrl'];

        // Load the javascript for resursCheckout plugin
        $this->document->addScript('catalog/view/javascript/resursCheckout/oc_resursCheckout.js');

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('extension/payment/resurs_checkout', $data));
    }

    /**
     * Controller for handling the display of partial payment.
     *
     * @return string
     * @throws Exception
     */
    public function partPayment()
    {
        $account = $this->getAccountByLanguage();
        $part_payment = $account['part_payment'];

        // Is part payment active?
        if (!$part_payment['active']) {
            return;
        }

        // Is the product id present in the GET request?
        if (!isset($this->request->get['product_id'])) {
            throw new \Exception('The product id needs to be present in GET for resurs part payment to work.');
        }

        // Get product info
        $this->load->model('catalog/product');
        $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);

        // Calculate the partial cost
        $price = $this->currency->format(
            $this->tax->calculate(
                $product_info['price'],
                $product_info['tax_class_id'],
                $this->config->get('config_tax')
            ),
            $this->session->data['currency'],
            '',
            false
        );

        $partial = round($price * (float)$part_payment['factor']);

        // Should we show it or not?
        if ($partial < (float)$part_payment['min_limit'] || $price > (float)$part_payment['max_limit']) {
            return;
        }

        $this->load->language('extension/payment/resurs_checkout');
        $infoLink = $this->url->link(
            'extension/payment/resurs_checkout/partialCostHtml',
            [
                'payment_method' => $part_payment['method'],
                'price' => $price,
            ]
        );

        // Return the html snippet
        ob_start();
        ?>
        <div class="resurs-partial-payment-container">
            <span class="partial-payment-text">
                <?php echo sprintf($this->language->get('partial_payment_from'), $partial) ?>
            </span>
            <a href="<?php echo $infoLink; ?>" target="_blank">
                <?php echo $this->language->get('partial_payment_info') ?>
            </a>
        </div>
        <?php

        return ob_get_clean();
    }

    /**
     * When the user clicks more info on partial payment we'll fetch the html from
     * Resurs and show it.
     *
     * @return void
     * @throws Exception
     */
    public function partialCostHtml()
    {
        if (!isset($this->request->get['payment_method']) || !isset($this->request->get['price'])) {
            throw new \Exception('To show the partial cost html we need the payment method and price in GET request.');
        }

        $paymentMethod = $this->request->get['payment_method'];
        $price = $this->request->get['price'];
        $connector = $this->connectorInstance($this->getAccountByLanguage());

        $html = $connector->getCostOfPurchase($paymentMethod, $price, true);

        $this->response->setOutput($html);
    }

    /**
     * Handle all of the callbacks
     *
     * @return void
     * @throws Exception
     */
    public function callbacks()
    {
        // Payload
        $action = $this->request->get['action'];
        $paymentId = $this->request->get['paymentId'];
        $result = isset($this->request->get['result']) ? $this->request->get['result'] : '';
        $digest = $this->request->get['digest'];

        $settings = $this->settings();
        $salt = $settings[$this->settingPrefix . 'salt'];

        $this->load->model('extension/payment/resurs_checkout');

        // Verify that the digest is correct.
        if (strtoupper(sha1($paymentId . $result . $salt)) != $digest) {
            $this->log->write(
                "[" . date('Y-m-d H:i:s') . "] Callback for resurs_checkout::" . $action .
                " had a missmatch in the digest. PaymentId: " . $paymentId . ", digest: " . $digest
            );
            http_response_code(406);
            exit();
        }

        // On each order there is meta saved on what language was used when the order was placed.
        // So we are using that meta to find out what account should be used.
        $account = $this->getAccountByOrderMeta($paymentId);
        $rcConnector = $this->connectorInstance($account);

        // If the callback type is booked there are some specific things to do.
        if ($action == 'booked' && $this->request->server['REQUEST_METHOD'] == 'POST') {
            // Set the payment method name
            $paymentData = $rcConnector->getPayment($paymentId);
            $this->model_extension_payment_resurs_checkout->setPaymentMethodName(
                $paymentId,
                $paymentData->paymentMethodName
            );

            // Add meta data
            if (!$rcConnector->addMetaData($paymentId, 'invoiceExtRef', (string)$paymentId)) {
                $this->log->write("[" . date('Y-m-d H:i:s') . "] Could not add meta to order with paymentId: " . $paymentId);
            }

            // Add the invoice fee if present.
            $jsonBody = file_get_contents('php://input');
            $body = json_decode($jsonBody, true);

            if (!is_null($body) && !empty($body['addedPaymentSpecificationLines'])) {
                $invoiceFee = $body['addedPaymentSpecificationLines'][0];
                $this->model_extension_payment_resurs_checkout->addInvoiceFee($paymentId, $invoiceFee);
            }
        }
        $paymentInfo = $rcConnector->getPayment($paymentId);
        $orderStatus = null;
        if (isset($paymentInfo, $paymentInfo->totalAmount)) {
            $this->model_extension_payment_resurs_checkout->setOrderTotal($paymentId, $paymentInfo->totalAmount);
            $orderStatus = $rcConnector->getOrderStatusByPayment($paymentInfo);
        }

        // Ask Resurs Bank what payment status this order have.
        $notifyCustomer = false;
        $extra = '';

        switch (true) {
            case $orderStatus & OrderStatus::PENDING:
                $newOrderId = $this->getOrderStatusId('on_hold');
                break;
            case $orderStatus & OrderStatus::PROCESSING:
                $newOrderId = $this->getOrderStatusId('processing');
                break;
            case $orderStatus & OrderStatus::COMPLETED:
                $newOrderId = $this->getOrderStatusId('completed');
                break;
            case $orderStatus & OrderStatus::ANNULLED:
                $newOrderId = $this->getOrderStatusId('cancelled');
                if ($action == 'annulment') {
                    $notifyCustomer = true;
                }
                break;
            case $orderStatus & OrderStatus::CREDITED:
                $newOrderId = $this->getOrderStatusId('refunded');
                break;
            default:
                $newOrderId = $this->getOrderStatusId('on_hold');
                $extra = sprintf("Default Action Used (%s).", 'on_hold');
                break;
        }

        $this->load->model('checkout/order');

        /** @var Proxy $orderModel */
        $orderModel = $this->model_checkout_order;
        $orderModel->addOrderHistory(
            $paymentId,
            $newOrderId,
            sprintf('Resurs Checkout callback types %s received at %s. %s', $action, date('Y-m-d H:i:s'), $extra),
            $notifyCustomer
        );
    }

    /**
     * Create a new order.
     *
     * @return void
     */
    public function createOrder()
    {
        $reference = $this->request->post['reference'];
        $customerData = $this->request->post['customer'];
        $paymentMethod = $this->request->post['paymentMethod'];
        $response = ['success' => true, 'redirect' => false];
        $orderData = [];

        $this->response->addHeader('Content-Type: application/json');

        // If the cart is empty, i.e session has expired. Just redirect to cart.
        if (!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) {
            $response = [
                'success' => true,
                'redirect' => true,
                'redirect_to' => $this->url->link('checkout/cart'),
            ];
            $this->response->setOutput(json_encode($response));
            return;
        }

        try {
            // Set order metaData
            $orderData['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            $orderData['store_id'] = $this->config->get('config_store_id');
            $orderData['store_name'] = $this->config->get('config_name');
            $orderData['language_id'] = $this->config->get('config_language_id');
            $orderData['currency_id'] = $this->currency->getId($this->session->data['currency']);
            $orderData['currency_code'] = $this->session->data['currency'];
            $orderData['currency_value'] = $this->currency->getValue($this->session->data['currency']);
            $orderData['ip'] = $this->request->server['REMOTE_ADDR'];
            $orderData['user_agent'] = isset($this->request->server['HTTP_USER_AGENT']) ?
                $this->request->server['HTTP_USER_AGENT'] :
                '';
            $orderData['accept_language'] = isset($this->request->server['HTTP_ACCEPT_LANGUAGE']) ?
                $this->request->server['HTTP_ACCEPT_LANGUAGE'] :
                '';
            $orderData['comment'] = '';

            if ($orderData['store_id']) {
                $orderData['store_url'] = $this->config->get('config_url');
            } else {
                $orderData['store_url'] = $this->request->server['HTTPS'] ? HTTPS_SERVER : HTTP_SERVER;
            }

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $orderData['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $orderData['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $orderData['forwarded_ip'] = '';
            }

            // Add the customer information
            $orderData['customer_id'] = $this->customer->isLogged() ? $this->customer->getId() : 0;
            $orderData['customer_group_id'] = $this->customer->isLogged() ? $this->customer->getGroupId() : 1;
            $orderData['firstname'] = $customerData['billingAddress']['firstName'];
            $orderData['lastname'] = $customerData['billingAddress']['lastName'];
            $orderData['email'] = $customerData['email'];
            $orderData['telephone'] = $customerData['phone'];
            $orderData['fax'] = '';
            $orderData['custom_field'] = [];

            // Add the payment information
            $orderData['payment_firstname'] = $customerData['billingAddress']['firstName'];
            $orderData['payment_lastname'] = $customerData['billingAddress']['lastName'];
            $orderData['payment_company'] = '';
            $orderData['payment_address_1'] = $customerData['billingAddress']['addressRow1'];
            $orderData['payment_address_2'] = $customerData['billingAddress']['addressRow2'];
            $orderData['payment_city'] = $customerData['billingAddress']['city'];
            $orderData['payment_postcode'] = $customerData['billingAddress']['postalCode'];
            $orderData['payment_zone'] = '';
            $orderData['payment_zone_id'] = '';
            $orderData['payment_country'] = '';
            $orderData['payment_country_id'] = '';
            $orderData['payment_address_format'] = '';
            $orderData['payment_custom_field'] = [
                'reference' => $reference,
                'for_language' => $this->session->data['language'],
            ];
            $orderData['payment_method'] = "Resurs Checkout - {$paymentMethod}";
            $orderData['payment_code'] = 'resurs_checkout';

            // Add the shipping information
            $shipKey = $customerData['deliveryAddress']['firstName'] ? 'deliveryAddress' : 'billingAddress';
            $orderData['shipping_firstname'] = $customerData[$shipKey]['firstName'];
            $orderData['shipping_lastname'] = $customerData[$shipKey]['lastName'];
            $orderData['shipping_company'] = '';
            $orderData['shipping_address_1'] = $customerData[$shipKey]['addressRow1'];
            $orderData['shipping_address_2'] = $customerData[$shipKey]['addressRow2'];
            $orderData['shipping_postcode'] = $customerData[$shipKey]['postalCode'];
            $orderData['shipping_city'] = $customerData[$shipKey]['city'];
            $orderData['shipping_zone_id'] = '';
            $orderData['shipping_zone'] = '';
            $orderData['shipping_zone_code'] = '';
            $orderData['shipping_country_id'] = '';
            $orderData['shipping_country'] = '';
            $orderData['shipping_address_format'] = '';
            $orderData['shipping_custom_field'] = [];
            $orderData['shipping_method'] = !empty($this->session->data['shipping_method']) ?
                $this->session->data['shipping_method']['title'] :
                '';
            $orderData['shipping_code'] = !empty($this->session->data['shipping_method']) ?
                $this->session->data['shipping_method']['code'] :
                '';

            // Collect the products.
            $orderData['products'] = [];
            foreach ($this->cart->getProducts() as $product) {
                $option_data = [];

                foreach ($product['option'] as $option) {
                    $option_data[] = [
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'option_id' => $option['option_id'],
                        'option_value_id' => $option['option_value_id'],
                        'name' => $option['name'],
                        'value' => $option['value'],
                        'type' => $option['type'],
                    ];
                }

                $orderData['products'][] = [
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'download' => $product['download'],
                    'quantity' => $product['quantity'],
                    'subtract' => $product['subtract'],
                    'price' => $product['price'],
                    'total' => $product['total'],
                    'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward' => $product['reward'],
                ];
            }

            // Collect the vouchers
            $orderData['vouchers'] = [];
            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $voucher) {
                    $orderData['vouchers'][] = [
                        'description' => $voucher['description'],
                        'code' => token(10),
                        'to_name' => $voucher['to_name'],
                        'to_email' => $voucher['to_email'],
                        'from_name' => $voucher['from_name'],
                        'from_email' => $voucher['from_email'],
                        'voucher_theme_id' => $voucher['voucher_theme_id'],
                        'message' => $voucher['message'],
                        'amount' => $voucher['amount'],
                    ];
                }
            }

            // Set the totals
            $totalsData = $this->getTotals();
            $orderData['totals'] = $totalsData['totals'];
            $orderData['total'] = $totalsData['total'];

            // Set the tracking data if it's present.
            if (isset($this->request->cookie['tracking'])) {
                $orderData['tracking'] = $this->request->cookie['tracking'];

                $subtotal = $this->cart->getSubTotal();

                // Affiliate
                $this->load->model('affiliate/affiliate');

                $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

                if ($affiliate_info) {
                    $orderData['affiliate_id'] = $affiliate_info['affiliate_id'];
                    $orderData['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
                } else {
                    $orderData['affiliate_id'] = 0;
                    $orderData['commission'] = 0;
                }

                // Marketing
                $this->load->model('checkout/marketing');

                $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

                if ($marketing_info) {
                    $orderData['marketing_id'] = $marketing_info['marketing_id'];
                } else {
                    $orderData['marketing_id'] = 0;
                }
            } else {
                $orderData['affiliate_id'] = 0;
                $orderData['commission'] = 0;
                $orderData['marketing_id'] = 0;
                $orderData['tracking'] = '';
            }

            // Add the order
            $this->load->model('checkout/order');
            $orderId = $this->model_checkout_order->addOrder($orderData);
            $this->session->data['order_id'] = $orderId;


            // If it's a zero sum order we are just adding the order and redirecting to the success page.
            // Otherwise we will add it like normal to Resurs.
            if ($this->cartTotal() <= 0) {
                $response['redirect'] = true;
                $response['redirect_to'] = $this->url->link('checkout/success');

                // Set the 0 order to status
                $this->load->model('checkout/order');
                $this->model_checkout_order->addOrderHistory(
                    $orderId,
                    $this->getOrderStatusId('on_hold'),
                    '0 sum order got handled by Resurs Checkout.'
                );
            } else {
                // Update the payment reference
                $account = $this->getAccountByLanguage();
                $rcConnector = $this->connectorInstance($account);
                $this->load->model('extension/payment/resurs_checkout');
                $this->model_extension_payment_resurs_checkout->setOrderTotal(
                    $orderId,
                    $this->cartTotal()
                );
                if (!$rcConnector->updatePaymentReference($reference, $orderId)) {
                    throw new Exception;
                }
            }
        } catch (Exception $e) {
            $this->load->language('extension/payment/resurs_checkout');
            // Set a session flash about the error.
            $this->session->data['error_flash_msg'] = $this->language->get('order_fail_critical_alert_message');
            $response = [
                'success' => false,
                'message' => $this->language->get('order_fail_critical_js_message'),
                'exception' => $e->getMessage(),
            ];
        }

        $this->response->setOutput(json_encode($response));
    }

    /**
     * Handle when the order fails in the iframe
     *
     * @return void
     */
    public function failOrder()
    {
        $reference = $this->request->post['reference'];

        // Load in the resurs model and find the order with the corresponding payment reference
        $this->load->model('extension/payment/resurs_checkout');
        $orderId = $this->model_extension_payment_resurs_checkout->orderIdByReference($reference);

        if (!is_null($orderId)) {
            $this->load->model('checkout/order');
            $this->model_checkout_order->deleteOrder($orderId);
        }

        // Set a session flash about the error.
        $this->load->language('extension/payment/resurs_checkout');
        $this->session->data['error_flash_msg'] = $this->language->get('order_fail_alert_message');

        $response = ['success' => true, 'message' => $this->language->get('order_fail_js_message')];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

    /**
     * Update the checkout iframe.
     *
     * @param string $reference
     * @return  boolean
     * @throws Exception
     */
    public function updateCheckout($reference)
    {
        $account = $this->getAccountByLanguage();
        $rcConnector = $this->connectorInstance($account);

        return $rcConnector->updateCheckoutOrderLines(
            $reference,
            $this->buildOrderSpecLines()
        );
    }

    /**
     * Get the products that's currently in the cart.
     * -- Copied from cart.php and modified
     *
     * @return array
     */
    private function getProducts()
    {
        $products = $this->cart->getProducts();
        $productsBuild = [];

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            $option_data = [];

            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = [
                    'name' => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
                ];
            }

            // Display prices
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $unit_price = $this->tax->calculate(
                    $product['price'],
                    $product['tax_class_id'],
                    $this->config->get('config_tax')
                );

                $price = $this->currency->format($unit_price, $this->session->data['currency']);
                $total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
            } else {
                $price = false;
                $total = false;
            }

            $recurring = '';

            if ($product['recurring']) {
                $frequencies = [
                    'day' => $this->language->get('text_day'),
                    'week' => $this->language->get('text_week'),
                    'semi_month' => $this->language->get('text_semi_month'),
                    'month' => $this->language->get('text_month'),
                    'year' => $this->language->get('text_year'),
                ];

                if ($product['recurring']['trial']) {
                    $recurring = sprintf(
                            $this->language->get('text_trial_description'),
                            $this->currency->format(
                                $this->tax->calculate(
                                    $product['recurring']['trial_price'] * $product['quantity'],
                                    $product['tax_class_id'],
                                    $this->config->get('config_tax')
                                ),
                                $this->session->data['currency']
                            ),
                            $product['recurring']['trial_cycle'],
                            $frequencies[$product['recurring']['trial_frequency']],
                            $product['recurring']['trial_duration']
                        ) . ' ';
                }

                if ($product['recurring']['duration']) {
                    $recurring .= sprintf(
                        $this->language->get('text_payment_description'),
                        $this->currency->format(
                            $this->tax->calculate(
                                $product['recurring']['price'] * $product['quantity'],
                                $product['tax_class_id'],
                                $this->config->get('config_tax')
                            ),
                            $this->session->data['currency']
                        ),
                        $product['recurring']['cycle'],
                        $frequencies[$product['recurring']['frequency']],
                        $product['recurring']['duration']
                    );
                } else {
                    $recurring .= sprintf(
                        $this->language->get('text_payment_cancel'),
                        $this->currency->format(
                            $this->tax->calculate(
                                $product['recurring']['price'] * $product['quantity'],
                                $product['tax_class_id'],
                                $this->config->get('config_tax')
                            ),
                            $this->session->data['currency']
                        ),
                        $product['recurring']['cycle'],
                        $frequencies[$product['recurring']['frequency']],
                        $product['recurring']['duration']
                    );
                }
            }

            $productsBuild[] = [
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'model' => $product['model'],
                'option' => $option_data,
                'recurring' => $recurring,
                'quantity' => $product['quantity'],
                'tax_class_id' => $product['tax_class_id'],
                'price' => $price,
                'total' => $total,
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
            ];
        }

        return $productsBuild;
    }

    /**
     * Get the vouchers for the order if there are any.
     * -- Copied from cart.php
     *
     * @return array
     */
    private function getVouchers()
    {
        $vouchers = [];

        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $key => $voucher) {
                $vouchers[] = [
                    'key' => $key,
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $this->session->data['currency']),
                ];
            }
        }

        return $vouchers;
    }

    /**
     * Get the totals for the order.
     * -- Copied from cart.php
     *
     * @return array
     */
    private function getTotals()
    {
        if (!is_null($this->totals)) {
            return $this->totals;
        }

        if (version_compare(VERSION, '3.0', '<')) {
            $this->load->model('extension/extension');
            $model_name = 'extension_extension';
        } else {
            $this->load->model('setting/extension');
            $model_name = 'setting_extension';
        }

        $totals = [];
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = [
            'totals' => &$totals,
            'taxes' => &$taxes,
            'total' => &$total,
        ];

        // Display prices
        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $sort_order = [];

            $results = $this->{'model_' . $model_name}->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = [];

            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
                $totals[$key]['text'] = $this->currency->format($value['value'], $this->session->data['currency']);
            }

            array_multisort($sort_order, SORT_ASC, $totals);
        }

        // Cache the totals for later use
        $this->totals = $total_data;

        return $total_data;
    }

    /**
     * Initialize the Resurs Checkout iFrame
     *
     * @return array
     * @throws Exception
     */
    private function rcoData()
    {
        try {
            $account = $this->getAccountByLanguage();
        } catch (Exception $e) {
            printf(
                "Checkout is currently unavailable. Reason: %s
                Plugin may not be properly configured. Please contact support and try again.",
                $e->getMessage()
            );
            exit;
        }
        $rcConnector = $this->connectorInstance($account);
        $this->rcoReference = $rcConnector->getPreferredPaymentId();

        $orderData = [
            'orderLines' => $this->buildOrderSpecLines(),
            'successUrl' => $this->url->link('checkout/success'),
            'backUrl' => $this->url->link('extension/payment/resurs_checkout'),
            'shopUrl' => $this->config->get('config_secure') ? HTTPS_SERVER : HTTP_SERVER,
        ];

        // Book payment and try to create the iframe
        try {
            if (!is_null($this->customer) && !is_null($this->customer->getAddressId())) {
                $this->load->model('account/address');
                $address = $this->model_account_address->getAddress($this->customer->getAddressId());

                if ($address && in_array($address['iso_code_2'], ['SE', 'NO', 'DK', 'FI'])) {
                    $rcConnector->setCustomer(
                        '',
                        '',
                        $this->customer->getTelephone(),
                        $this->customer->getEmail(),
                        'NATURAL',
                        ''
                    );
                    $rcConnector->setDeliveryAddress(
                        $address['firstname'] . ' ' . $address['lastname'],
                        $address['firstname'],
                        $address['lastname'],
                        $address['address_1'],
                        $address['address_2'],
                        $address['city'],
                        $address['postcode'],
                        $address['iso_code_2']
                    );
                }
            }

            $html = $rcConnector->createPayment($this->rcoReference, $orderData);
            $full = $rcConnector->getFullCheckoutResponse();
            preg_match("<script.+?src=[\"'](.+?)[\"'?].*?>", $html, $matches);

            return [
                'paymentSessionId' => $full->paymentSessionId,
                'baseUrl' => $full->baseUrl,
                'scriptUrl' => $matches[1],
                'html' => $html,
            ];
        } catch (Exception $e) {
            var_dump($e->getMessage());
            die("The checkout is not available right now. Please try again in a minute.");
        }
    }

    /**
     * Build up the order speclines to send to Resurs.
     *
     * @return array
     */
    private function buildOrderSpecLines()
    {
        $specLines = [];
        $productsIncVat = 0.0;
        $freeShipping = false;

        // Build up speclines for the products
        foreach ($this->cart->getProducts() as $product) {
            // Get the tax rate for the product.
            $vatPct = 0;
            foreach ($this->tax->getRates($product['price'], $product['tax_class_id']) as $rate) {
                if ($rate['type'] == 'P') {
                    $vatPct += (float)$rate['rate'];
                }
            }

            $specLines[] = [
                'artNo' => $product['product_id'],
                'description' => $product['name'],
                'quantity' => (float)$product['quantity'],
                'unitMeasure' => 'st',
                'unitAmountWithoutVat' => $this->currency->format(
                    $product['price'],
                    $this->session->data['currency'],
                    '',
                    false
                ),
                'vatPct' => $vatPct,
                'type' => 'ORDER_LINE',
            ];

            $productsIncVat += $this->currency->format(
                $this->tax->calculate(
                    $product['price'] * $product['quantity'],
                    $product['tax_class_id'],
                    $this->config->get('config_tax')
                ),
                $this->session->data['currency'],
                '',
                false
            );
        }

        // Add the vouchers
        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $key => $voucher) {
                $specLines[] = [
                    'artNo' => 'Gift certificate',
                    'description' => $voucher['description'],
                    'quantity' => 1,
                    'unitMeasure' => 'st',
                    'unitAmountWithoutVat' => $this->currency->format(
                        $voucher['amount'],
                        $this->session->data['currency'],
                        '',
                        false
                    ),
                    'vatPct' => 0,
                    'type' => 'ORDER_LINE',
                ];
            }
        }

        // Add the use of coupon codes
        $totals = $this->getTotals();
        foreach ($totals['totals'] as $total) {
            if ($total['code'] == 'coupon') {
                $this->load->model('extension/total/coupon');
                $coupon_info = $this->model_extension_total_coupon->getCoupon($this->session->data['coupon']);

                if ($coupon_info) {
                    // Is the coupon a percentage?
                    if ($coupon_info['type'] == 'P') {
                        // Is it free shipping?
                        if ((bool)$coupon_info['shipping']) {
                            $freeShipping = true;
                            break;
                        }

                        $couponPct = ((float)$coupon_info['discount']) / 100;
                        $discountTotal = -($productsIncVat * $couponPct);
                    } else {
                        $discountTotal = 0.0;
                    }

                    $specLines[] = [
                        'artNo' => 'coupon',
                        'description' => $total['title'],
                        'quantity' => 1,
                        'unitMeasure' => 'st',
                        'unitAmountWithoutVat' => $discountTotal,
                        'vatPct' => 0.0,
                        'type' => 'DISCOUNT',
                    ];
                }
            }
        }

        // Add the use of a gift card
        foreach ($totals['totals'] as $total) {
            if ($total['code'] == 'voucher') {
                $specLines[] = [
                    'artNo' => 'Gift card',
                    'description' => $total['title'],
                    'quantity' => 1,
                    'unitMeasure' => 'st',
                    'unitAmountWithoutVat' => $this->currency->format(
                        $total['value'],
                        $this->session->data['currency'],
                        '',
                        false
                    ),
                    'vatPct' => 0.0,
                    'type' => 'DISCOUNT',
                ];
            }
        }

        // Add shipping fee
        if (
            $this->cart->hasShipping() &&
            isset($this->session->data['shipping_method']) &&
            !empty($this->session->data['shipping_method']) &&
            !$freeShipping
        ) {
            // Calculate the vat percentage for shipping
            $vatPct = 0;
            foreach (
                $this->tax->getRates(
                    $this->session->data['shipping_method']['cost'],
                    $this->session->data['shipping_method']['tax_class_id']
                ) as $rate
            ) {
                if ($rate['type'] == 'P') {
                    $vatPct += (float)$rate['rate'];
                }
            }

            $specLines[] = [
                'artNo' => 'Shipping',
                'description' => $this->session->data['shipping_method']['title'],
                'quantity' => 1,
                'unitMeasure' => 'st',
                'unitAmountWithoutVat' => $this->currency->format(
                    $this->session->data['shipping_method']['cost'],
                    $this->session->data['currency'],
                    '',
                    false
                ),
                'vatPct' => $vatPct,
                'type' => 'SHIPPING_FEE',
            ];
        }

        return $specLines;
    }

    /**
     * Get the settings for the plugin
     *
     * @return array
     */
    private function settings()
    {
        if (!is_null($this->settings)) {
            return $this->settings;
        }

        // Load the settings model
        $this->load->model('setting/setting');

        $this->settings = $this->model_setting_setting->getSetting('resurs_checkout');

        return $this->settings;
    }

    /**
     * Get the order status id set in admin for a specific resurs payment status.
     */
    /**
     * Get the order status id set in admin for a specific resurs payment status.
     *
     * @param string $status
     * @return  array
     */
    private function getOrderStatusId($status)
    {
        $settings = $this->settings();

        return $settings[$this->settingPrefix . 'settings_order_statuses_' . $status];
    }

    /**
     * Get the total value of the cart. Products + vouchers + shipping - discount - giftcard.
     *
     * @return void
     */
    private function cartTotal()
    {
        $productsTotal = 0.0;
        foreach ($this->cart->getProducts() as $product) {
            $productsTotal += $this->currency->format(
                $this->tax->calculate(
                    $product['price'],
                    $product['tax_class_id'],
                    $this->config->get('config_tax')
                ) * $product['quantity'],
                $this->session->data['currency'],
                '',
                false
            );
        }

        $vouchersTotal = 0.0;
        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $key => $voucher) {
                $vouchersTotal += $this->currency->format(
                    $voucher['amount'],
                    $this->session->data['currency'],
                    '',
                    false
                );
            }
        }

        $shippingTotal = 0.0;
        if (
            $this->cart->hasShipping() &&
            isset($this->session->data['shipping_method']) &&
            !empty($this->session->data['shipping_method'])
        ) {
            $shippingTotal += $this->currency->format(
                $this->tax->calculate(
                    $this->session->data['shipping_method']['cost'],
                    $this->session->data['shipping_method']['tax_class_id'],
                    $this->config->get('config_tax')
                ),
                $this->session->data['currency'],
                '',
                false
            );
        }

        $giftcardTotal = 0.0;
        $totals = $this->getTotals();
        foreach ($totals['totals'] as $total) {
            if ($total['code'] == 'voucher') {
                $giftcardTotal = $this->currency->format($total['value'], $this->session->data['currency'], '', false);
            }
        }

        // + giftcardTotal because it returns a negative value.
        $cartTotal = $productsTotal + $vouchersTotal + $shippingTotal + $giftcardTotal;

        return $cartTotal;
    }

    /**
     * Create a connector instance to Resurs Bank
     *
     * @param array $account
     * @return  ResursBank
     * @throws ReflectionException
     */
    private function connectorInstance($account)
    {
        if (!is_null($this->connector)) {
            return $this->connector;
        }

        $settings = $this->settings();
        $salt = $settings[$this->settingPrefix . 'salt'];
        $environment = $settings[$this->settingPrefix . 'environment'];

        $connector = new ResursBank($account['username'], $account['password']);
        $connector->setCallbackDigest($salt);
        $connector->setUserAgent(
            sprintf(
                '+OpenCart-%s ResursBank-%s',
                VERSION,
                (new Generic())->getVersionByClassDoc(__CLASS__)
            )
        );
        $connector->setEnvironment(
            $environment == 'test' ?
                ResursEnvironments::TEST :
                ResursEnvironments::PRODUCTION
        );
        $connector->setPreferredPaymentService(RESURS_FLOW_TYPES::RESURS_CHECKOUT);

        // Set the url for checkout javascript (will be moved to resursCheckout.js)
        $this->rcoJsUrl = $connector->getCheckoutUrl();

        // Cache the connector
        $this->connector = $connector;

        return $connector;
    }

    /**
     * Get the account based on the current selected language.
     *
     * @return array
     * @throws Exception
     */
    private function getAccountByLanguage()
    {
        $settings = $this->settings();
        $accounts = $settings[$this->settingPrefix . 'accounts'];

        foreach ($accounts as $account) {
            if ($this->session->data['language'] == $account['for_language'] && $account['active']) {
                return $account;
            }
        }

        // No match for a language.
        throw new Exception('There was no language matched with an account.');
    }

    /**
     * @throws Exception
     */
    private function getProperModel()
    {
        $modelError = false;
        try {
            $this->load->model('extension/payment/resurs');
        } catch (Exception $e) {
            $modelError = true;
        }
        if ($modelError) {
            $this->load->model('extension/payment/resurs_checkout');
        }
    }

    /**
     * Get the account based on the meta data on the order. When we are placing an order we are saving
     * a meta of which language was used.
     *
     * @param string $paymentId
     * @return  array
     * @throws  Exception
     */
    private function getAccountByOrderMeta($paymentId)
    {
        // Note: Some of the actions that takes place in this method are shared with callbacks.
        // When callbacks are used, language are less important, which we also bypass when errors occurs.
        // Also, this might happen as a conflict between different opencart versions.
        $orderLanguage = null;
        $this->getProperModel();

        if (method_exists($this->model_extension_payment_resurs, 'orderLanguageFromMeta')) {
            $orderLanguage = $this->model_extension_payment_resurs->orderLanguageFromMeta($paymentId);
        }

        $settings = $this->settings();
        $accounts = $settings[$this->settingPrefix . 'accounts'];

        foreach ($accounts as $account) {
            if ($orderLanguage == $account['for_language'] || is_null($orderLanguage)) {
                return $account;
            }
        }

        $this->log->write(
            "[" . date('Y-m-d H:i:s') . "] Callback for resurs_checkout::" . $action .
            " could not find the language by order meta. PaymentId: " . $paymentId . ", digest: " . $digest
        );
        throw new Exception('There was no language matched with an account from language meta');
    }
}
