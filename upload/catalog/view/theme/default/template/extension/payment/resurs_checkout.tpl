<?php echo $header; ?>
<style>
    #rco-container {
        margin-top: 20px;
        padding-top: 20px;
        border-top: 1px solid #ddd;
        min-height: 400px;
    }
</style>
<script type="text/javascript" defer src="<?php echo $rco['scriptUrl']; ?>"></script>
<script>
    var REFERENCE = "<?php echo $reference; ?>";
    var BASE_URL = "<?php echo $rco['baseUrl']; ?>";
    var PAYMENT_SESSION_ID = "<?php echo $rco['paymentSessionId']; ?>";
    var AJAX_CREATE_ORDER_URL = "<?php echo $ajax_createOrderUrl; ?>";
    var AJAX_FAIL_ORDER_URL = "<?php echo $ajax_failOrderUrl; ?>";
</script>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) : ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php endforeach; ?>
    </ul>

    <div class="row">
        <div class="col-xs-12">
            <h2><?php echo $confirm_order; ?></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td class="text-left"><?php echo $column_name; ?></td>
                            <td class="text-left"><?php echo $column_model; ?></td>
                            <td class="text-center"><?php echo $column_quantity; ?></td>
                            <td class="text-right"><?php echo $column_price; ?></td>
                            <td class="text-right"><?php echo $column_total; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product) : ?>
                            <tr>
                                <td class="text-left">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <?php if ($product['option']) : ?>
                                        <?php foreach ($product['option'] as $option) : ?>
                                            <br />
                                            <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </td>
                                <td class="text-left"><?php echo $product['model']; ?></td>
                                <td class="text-center"><?php echo $product['quantity']; ?></td>
                                <td class="text-right"><?php echo $product['price']; ?></td>
                                <td class="text-right"><?php echo $product['total']; ?></td>
                            </tr>
                        <?php endforeach; ?>

                        <?php foreach ($vouchers as $voucher) : ?>
                            <tr>
                                <td class="text-left"><?php echo $voucher['description']; ?></td>
                                <td class="text-left"></td>
                                <td class="text-left"></td>
                                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                            </tr>
                        <?php endforeach; ?>

                        <?php foreach ($totals['totals'] as $total) : ?>
                            <tr>
                                <td colspan="4" class="text-right"><?php echo $total['title']; ?></td>
                                <td class="text-right"><?php echo $total['text']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_shopping; ?></a>
            </div>
        </div>
    </div>

    <?php if (isset($error_flash_msg) && !empty($error_flash_msg)) : ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger" style="margin: 20px 0 0 0">
                    <i class="fa fa-exclamation-circle"></i> <?php echo $error_flash_msg; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-xs-12">
            <div id="rco-container"></div>
        </div>
    </div>

</div>
<?php echo $footer; ?>