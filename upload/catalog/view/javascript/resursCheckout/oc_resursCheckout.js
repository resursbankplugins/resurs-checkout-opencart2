var rcoCustomer = {}
var rcoPaymentMethod = '';

jQuery(document).ready(function ($) {
    initalizeResursCheckout();
});

function initalizeResursCheckout() {
    $ResursCheckout.onCustomerChange(function (event) {
        rcoCustomer = event
    });

    $ResursCheckout.onPaymentChange(function (event) {
        rcoPaymentMethod = event.method
    });

    $ResursCheckout.onSubmit(function (event) {
        $.post(AJAX_CREATE_ORDER_URL, {
            customer: rcoCustomer,
            paymentMethod: rcoPaymentMethod,
            reference: REFERENCE
        })
            .done(function (response) {
                if (response.success) {
                    if (response.redirect) {
                        location.replace(response.redirect_to);
                    } else {
                        $ResursCheckout.release();
                    }
                } else {
                    alert(response.message);
                    location.reload();
                }
            });
    });

    $ResursCheckout.onPaymentFail(function (event) {
        $.post(AJAX_FAIL_ORDER_URL, {
            reference: REFERENCE
        })
            .done(function (response) {
                alert(response.message);
                location.reload();
            });
    });

    $ResursCheckout.create({
        paymentSessionId: PAYMENT_SESSION_ID,
        baseUrl: BASE_URL,
        hold: true,
        containerId: 'resurs-checkout-container'
    });
}
