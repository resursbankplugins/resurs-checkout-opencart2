<?php

class ModelExtensionPaymentResursCheckout extends Model
{

    public function getMethod($address, $total)
    {
        // Not shown in the payment method list
        return [];
    }


    /**
     * Get an order ID based on the payment reference for resurs checkout.
     */
    public function orderIdByReference($reference)
    {
        $orders = $this->db->query(
            "SELECT order_id, payment_custom_field FROM " .
            DB_PREFIX . "order WHERE payment_code = 'resurs_checkout'"
        );

        foreach ($orders->rows as $order) {
            $paymentCustom = json_decode($order['payment_custom_field']);

            if ($paymentCustom && $paymentCustom->reference == $reference) {
                return $order['order_id'];
            }
        }

        return null;
    }

    /**
     * Get the language used on the order from the payment meta
     *
     * @param string $reference
     * @return  string
     */
    public function orderLanguageFromMeta($reference)
    {
        $order = $this->db->query(
            "SELECT payment_custom_field FROM " . DB_PREFIX . "order WHERE order_id = " . $reference
        );

        if ($order->num_rows > 0) {
            $paymentCustom = json_decode($order->row['payment_custom_field']);

            if (isset($paymentCustom->for_language)) {
                return $paymentCustom->for_language;
            }
        }

        return null;
    }

    public function setPaymentMethodName($orderId, $name)
    {
        $this->db->query("
            UPDATE `" . DB_PREFIX . "order`
            SET `payment_method` = concat(payment_method, ' - " . $name . "')
            WHERE `order_id` = " . $this->db->escape($orderId));
    }

    /**
     * @param $orderId
     * @param $orderTotal
     */
    public function setOrderTotal($orderId, $orderTotal)
    {
        $result = $this->db->query(
            "SELECT total FROM " . DB_PREFIX . "order WHERE order_id = " . $this->db->escape($orderId)
        );

        if ($result->num_rows === 1) {
            $totalRow = array_pop($result->rows);
            if ((float)$totalRow['total'] === 0.00 && (float)$orderTotal > 0 && (float)$totalRow['total'] !== (float)$orderTotal) {
                $this->db->query(
                    sprintf(
                        "UPDATE `" . DB_PREFIX . "order` SET total = '%s'
                        WHERE `order_id` = '%s'",
                        floatval($orderTotal),
                        $this->db->escape($orderId)
                    )
                );
            }
        }
    }

    /**
     * If the order has an invoice fee then we add it.
     */
    public function addInvoiceFee($orderId, $invoiceFee)
    {
        // Insert the invoice fee first.
        $this->db->query("
            INSERT INTO `" . DB_PREFIX . "order_total` (`order_id`, `code`, `title`, `value`, `sort_order`)
            VALUES(" . $orderId . ", 'resurs_fee', '" . $this->db->escape($invoiceFee['description']) .
            "', " . $invoiceFee['unitAmountWithoutVat'] . ", 4)
        ");

        // Update the sub total
        $this->db->query("
            UPDATE `" . DB_PREFIX . "order_total`
            SET `value` = value + " . $invoiceFee['unitAmountWithoutVat'] . "
            WHERE `order_id` = " . $this->db->escape($orderId) . "
            AND `code` = 'sub_total'
        ");

        // Update the VAT (add the invoice fee vat)
        $this->db->query("
            UPDATE `" . DB_PREFIX . "order_total`
            SET `value` = value + " . $invoiceFee['totalVatAmount'] . "
            WHERE `order_id` = " . $this->db->escape($orderId) . "
            AND `code` = 'tax'
        ");

        // Update the totals.
        $this->db->query("
            UPDATE `" . DB_PREFIX . "order_total`
            SET `value` = value + " . $invoiceFee['totalAmount'] . "
            WHERE `order_id` = " . $this->db->escape($orderId) . "
            AND `code` = 'total'
        ");

        // Update the order total on the order
        $this->db->query("
            UPDATE `" . DB_PREFIX . "order`
            SET `total` = total + " . $invoiceFee['totalAmount'] . "
            WHERE `order_id` = " . $this->db->escape($orderId));
    }

    /**
     * Has this order got the booked callback yet?
     */
    public function bookedCallbackArrived($orderId)
    {
        $result = $this->db->query(
            "SELECT order_id FROM " . DB_PREFIX . "order_history WHERE order_id = " . $this->db->escape($orderId) .
            " AND comment LIKE '%booked%'"
        );

        return $result->num_rows > 0 ? true : false;
    }
}
