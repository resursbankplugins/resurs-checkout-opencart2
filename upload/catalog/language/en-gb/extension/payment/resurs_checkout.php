<?php

$_['resurs_checkout_title'] = 'Resurs Checkout';
$_['confirm_order'] = 'Confirm your order';
$_['order_fail_js_message'] = 'The payment failed. Please choose another payment method and try again after the page has been reloaded.';
$_['order_fail_alert_message'] = 'The payment failed. Please choose another payment method and try again.';
$_['order_fail_critical_js_message'] = 'Something went wrong. Please try again after the page has been reloaded.';
$_['order_fail_critical_alert_message'] = 'Something went wrong. Please try again.';

$_['partial_payment_from'] = "Pay from %s:-/month";
$_['partial_payment_info'] = "Info";