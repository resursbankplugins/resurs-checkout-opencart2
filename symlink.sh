#!/bin/sh

if [ "" = "$1" ] ; then
  echo "Usage: $0 <SITE_ROOT>"
  exit 1
fi

#PLUGIN_ROOT='resurs-checkout-opencart2/upload'
PLUGIN_ROOT="$(pwd -P)/upload"
SITE_ROOT="$1"

if [ ! -d $SITE_ROOT/catalog/view/javascript/resursCheckout ] ; then
	mkdir -v $SITE_ROOT/catalog/view/javascript/resursCheckout
fi

# Admin
ln -fs "$PLUGIN_ROOT/admin/controller/extension/payment/resurs_checkout.php" "$SITE_ROOT/admin/controller/extension/payment/resurs_checkout.php"
ln -fs "$PLUGIN_ROOT/admin/language/en-gb/extension/payment/resurs_checkout.php" "$SITE_ROOT/admin/language/en-gb/extension/payment/resurs_checkout.php"
ln -fs "$PLUGIN_ROOT/admin/view/image/payment/resurs_checkout.png" "$SITE_ROOT/admin/view/image/payment/resurs_checkout.png"
ln -fs "$PLUGIN_ROOT/admin/view/template/extension/payment/resurs_checkout.tpl" "$SITE_ROOT/admin/view/template/extension/payment/resurs_checkout.tpl"
ln -fs "$PLUGIN_ROOT/admin/view/template/extension/payment/resurs_checkout.twig" "$SITE_ROOT/admin/view/template/extension/payment/resurs_checkout.twig"
# Catalog
ln -fs "$PLUGIN_ROOT/catalog/controller/extension/payment/resurs_checkout.php" "$SITE_ROOT/catalog/controller/extension/payment/resurs_checkout.php"
ln -fs "$PLUGIN_ROOT/catalog/language/en-gb/extension/payment/resurs_checkout.php" "$SITE_ROOT/catalog/language/en-gb/extension/payment/resurs_checkout.php"
ln -fs "$PLUGIN_ROOT/catalog/model/extension/payment/resurs_checkout.php" "$SITE_ROOT/catalog/model/extension/payment/resurs_checkout.php"
ln -fs "$PLUGIN_ROOT/catalog/view/javascript/resursCheckout/oc_resursCheckout.js" "$SITE_ROOT/catalog/view/javascript/resursCheckout/oc_resursCheckout.js"
ln -fs "$PLUGIN_ROOT/catalog/view/javascript/resursCheckout/resursCheckout.js" "$SITE_ROOT/catalog/view/javascript/resursCheckout/resursCheckout.js"
ln -fs "$PLUGIN_ROOT/catalog/view/theme/default/template/extension/payment/resurs_checkout.tpl" "$SITE_ROOT/catalog/view/theme/default/template/extension/payment/resurs_checkout.tpl"
ln -fs "$PLUGIN_ROOT/catalog/view/theme/default/template/extension/payment/resurs_checkout.twig" "$SITE_ROOT/catalog/view/theme/default/template/extension/payment/resurs_checkout.twig"
# System
#rm "$SITE_ROOT/system/library/resurs-ecomphp"
#cp -R "$PLUGIN_ROOT/system/library/resurs-ecomphp" "$SITE_ROOT/system/library/resurs-ecomphp"
ln -fs "$PLUGIN_ROOT/system/library/resurs-ecomphp" "$SITE_ROOT/system/library/resurs-ecomphp"
